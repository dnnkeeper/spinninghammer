﻿/// 'Getting Over It'-like controller attempt
/// v 0.5
/// dnnkeeper 2017
/// https://bitbucket.org/dnnkeeper/spinninghammer

using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class HammerController : MonoBehaviour {

    public enum HammerMode
    {
        TranslateChild,
        UseTargetPosition
    }

    public float rotationSpeed = 5.0f;

    public float jointSpeed = 10.0f;
    
    public float minRadius = 0.5f;

    public float maxRadius = 2f;

    public HammerMode jointMode;

    ConfigurableJoint _cJoint;

    Rigidbody _rb;

    Vector3 _childLocalPos;

    Vector3 _clickPos;

    Vector3 _bodyToClickPos;

    // Use this for initialization
    void Start () {
        
        _rb = GetComponent<Rigidbody>();

        _cJoint = GetComponent<ConfigurableJoint>();

        _bodyToClickPos = _cJoint.connectedAnchor;
        
        _childLocalPos = transform.GetChild(0).transform.localPosition;
    }
    Vector3 mousePos;

    void OnEnable()
    {
        mousePos = Input.mousePosition;
    }
    
    // Update is called once per frame
    void Update ()
    {
        mousePos = Input.mousePosition;
        
        Camera mainCam = Camera.main;
        
        //Project mouse position to object plane
        Plane objectPlane = new Plane(mainCam.transform.forward, _cJoint.transform.position);
        Ray clickRay = mainCam.ScreenPointToRay(mousePos);
        float distance = 0f;
        if ( objectPlane.Raycast(clickRay, out distance) )
        {
            _clickPos = clickRay.origin + clickRay.direction * distance;
        }

        Vector3 bodyToClickPos = Vector3.ClampMagnitude(_clickPos - _cJoint.connectedBody.transform.position, maxRadius);
        if (bodyToClickPos.sqrMagnitude < minRadius* minRadius ) {
            _bodyToClickPos = _bodyToClickPos.normalized * minRadius;
        }
        _bodyToClickPos = Vector3.Lerp(_bodyToClickPos, bodyToClickPos, jointSpeed * Time.deltaTime);
        _childLocalPos = -(Vector3.up * _bodyToClickPos.magnitude / _cJoint.transform.localScale.y);

        Quaternion rot = Quaternion.Inverse(Quaternion.LookRotation(_cJoint.connectedBody.transform.forward, -_bodyToClickPos.normalized));
        
        _cJoint.angularZMotion = ConfigurableJointMotion.Free;

        _cJoint.targetRotation = Quaternion.Lerp(_cJoint.targetRotation, rot, rotationSpeed * Time.deltaTime);
        
        switch (jointMode)
        {
            case HammerMode.TranslateChild:

                _cJoint.yMotion = ConfigurableJointMotion.Locked;

                _cJoint.transform.GetChild(0).localPosition = _childLocalPos;

                break;

            case HammerMode.UseTargetPosition:

                _cJoint.yMotion = ConfigurableJointMotion.Free;

                _cJoint.transform.GetChild(0).localPosition = Vector3.zero;

                _cJoint.targetPosition = -(rot * _bodyToClickPos);

                break;

            default:

                break;
        }
        
        Debug.DrawRay(_cJoint.connectedBody.transform.position, _cJoint.transform.rotation * -Vector3.up , Color.cyan);

        _rb.WakeUp();

        Debug.DrawRay(_cJoint.connectedBody.transform.position, _bodyToClickPos, Color.red);
    }
}
